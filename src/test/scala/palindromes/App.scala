package palindromes
// --- Directions
// Given a string, return true if the string is a palindrome
// or false if it is not.  Palindromes are strings that
// form the same word if it is reversed. *Do* include spaces
// and punctuation in determining if the string is a palindrome.
// --- Examples:
//   palindrome("abba") === true
//   palindrome("abcdefg") === false
object App {

  def palindrome(str: String): Boolean = str == reverse(str)

  def reverse(str: String): String = {
    var reversed = ""

    for(i <- 0 to str.length - 1) {
      reversed = str.charAt(i) + reversed
    }

    return reversed
  }

}
