package palindromes

import org.scalatest.{FlatSpec, Matchers}

class Test extends FlatSpec with Matchers {

  import App.palindrome

  it should "'aba' is a palindrome" in {
    palindrome("aba") should be (true)
  }

  it should "' aba' is not a palindrome" in {
    palindrome(" aba") should be (false)
  }

  it should "'aba ' is not a palindrome" in {
    palindrome("aba ") should be (false)
  }

  it should "'greetings' is not a palindrome" in  {
    palindrome("greetings") should be (false)
  }

  it should "'1000000001' a palindrome" in  {
    palindrome("1000000001") should be (true)
  }

  it should "'Fish hsif' is not a palindrome" in  {
    palindrome("Fish hsif") should be (false)
  }
  
  it should "'pennep' a palindrome" in {
    palindrome("pennep") should be (true)
  }

}
