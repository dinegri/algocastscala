package fib

import org.scalatest.{FlatSpec, Matchers}

class Test extends FlatSpec with Matchers {

  import fib.App._

  it should "calculates correct fib value for 1" in {
    fib(1) should be (1)
  }

  it should "calculates correct fib value for 2" in {
    fib(2) should be (1)
  }

  it should "calculates correct fib value for 3" in {
    fib(3) should be (2)
  }

  it should "calculates correct fib value for 4" in {
    fib(4) should be (3)
  }

  it should "calculates correct fib value for 39" in {
    fib(39) should be (63245986)
  }

}
