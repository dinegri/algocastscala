package fib
// --- Directions
// Print out the n-th entry in the fibonacci series.
// The fibonacci series is an ordering of numbers where
// each number is the sum of the preceeding two.
// For example, the sequence
//  [0, 1, 1, 2, 3, 5, 8, 13, 21, 34]
// forms the first ten entries of the fibonacci series.
// Example:
//   fib(4) === 3
object App {

  def fib(n: Int): Int = {
    var acc     = 1
    var prevacc = 0
    var prev    = 0

    for (i <- 1 until n) {
      prev = prevacc
      prevacc = acc
      acc = prev + acc
    }

    acc
  }

}
