package reversestring

import org.scalatest.{FlatSpec, Matchers}

class Test extends FlatSpec with Matchers {

  import reversestring.App.reverse

  it should "Reverse reverses a string" in {
    reverse("abcd") should be ("dcba")
  }

  it should "Reverse reverses a string with empty character" in {
    reverse("  abcd") should be ("dcba  ")
  }


}
