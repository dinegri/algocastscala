package reversestring
// --- Directions
// Given a string, return a new string with the reversed
// order of characters
// --- Examples
//   reverse('apple') === 'leppa'
//   reverse('hello') === 'olleh'
//   reverse('Greetings!') === '!sgniteerG'
object App {

  def reverse(str: String): String = {
    var reversed = ""

    for(i <- 0 to str.length - 1) {
      reversed = str.charAt(i) + reversed
    }

    return reversed
  }

}
