package reverseint
// --- Directions
// Given an integer, return an integer that is the reverse
// ordering of numbers.
// --- Examples
//   reverseInt(15) === 51
//   reverseInt(981) === 189
//   reverseInt(500) === 5
//   reverseInt(-15) === -51
//   reverseInt(-90) === -9
object App {

  def reverseInt(n: Int): Int = n match {
    case n if n < 0 => String.valueOf(n * -1).reverse.toInt * -1
    case n if n > 1 => String.valueOf(n).reverse.toInt
  }

  def reverseIntV1(n: Int): Int = {
    def sign(n: Int) = if (n > 0) 1 else -1

    val nc = if (n < 0) n * -1 else n
    String.valueOf(nc).reverse.toInt * sign(n)
  }



}
