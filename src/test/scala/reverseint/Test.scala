package reverseint

import org.scalatest.{FlatSpec, Matchers}

class Test extends FlatSpec with Matchers {

  import App.reverseInt

  it should "any description" in {
    reverseInt(-15) should be (-51)
  }

}
